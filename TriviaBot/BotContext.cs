﻿using System;
using System.Collections.Generic;
using System.Text;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.VoiceNext;
using TriviaBot.Plugins;
using DSharpPlus.Lavalink;
using Microsoft.Extensions.Configuration;

namespace TriviaBot 
{
    class BotContext : IBotPluginContext
    {
        private DiscordClient discord;
        private InteractivityExtension interactivity;
        private CommandsNextExtension commands;
        private LavalinkNodeConnection lava;
        private IConfiguration config;

        public BotContext(DiscordClient discord, InteractivityExtension interactivity, CommandsNextExtension commands, LavalinkNodeConnection lava, IConfiguration config)
        {
            this.discord = discord;
            this.interactivity = interactivity;
            this.commands = commands;
            this.lava = lava;
            this.config = config;
        }

        public DiscordClient GetDiscordClient => discord;

        public InteractivityExtension GetInteractivityExtension => interactivity;

        public CommandsNextExtension GetCommandsNextExtension => commands;

        public LavalinkNodeConnection GetLavalinkNodeConnection => lava;

        public IConfiguration GetConfiguration => config;
    }
}
