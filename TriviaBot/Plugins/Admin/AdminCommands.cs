﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TriviaBot.Plugins.Admin.ChannelSimulator;

namespace TriviaBot.Admin
{
    class AdminCommands : BaseCommandModule
    {
       
        [Command("export")]
        public async Task Test(CommandContext ctx)
        {
            Console.Out.WriteLine("Start.");
            
            foreach (var g in ctx.Client.Guilds)
            {
                foreach(var c in g.Value.Channels)
                {
                    if(c.Value.Type == DSharpPlus.ChannelType.Text)
                    {
                        await ExportMessagesForGuild(g.Value);
                    }                   
                }
            }
            

            Console.Out.WriteLine("Done.");
            //Console.Out.WriteLine(output);
        }


        /*
        [Command("update")]
        public async Task Update(CommandContext ctx)
        {
            Console.Out.WriteLine("Start Update");

            foreach (var g in ctx.Client.Guilds)
            {
                await UpdateChainModels(g.Value);

            }


            Console.Out.WriteLine("Done.");
            //Console.Out.WriteLine(output);
        }
        */

        [Command("simulate_topic")]
        public async Task Generate(CommandContext ctx, string channelName, string outputChannelName = null)
        {
            await Generate(ctx, channelName, outputChannelName, null);
        }

        [Command("simulate_channel")]
        public async Task Generate(CommandContext ctx, string channelName, string outputChannelName = null, string topic = null)
        {
            
            ChannelSimulator simulator = null;
            DiscordChannel outputChannel = null;

            foreach(var guild in ctx.Client.Guilds.Values)
            {
                foreach(var c in guild.Channels)
                {
                    if (c.Value.Name.ToLower() == channelName.ToLower() && simulator == null)
                    {
                        simulator = new ChannelSimulator(c.Value);
                    }
                    if(c.Value.Name.ToLower() == outputChannelName.ToLower())
                    {
                        outputChannel = c.Value;
                    }
                }
            }

            if(simulator != null)
            {
                await ctx.Client.SendMessageAsync(outputChannel, "Current Topic is [" + topic + "]\n\nPlease wait while I update my models.");

                Random rnd = new Random();
                await simulator.UpdateModels(topic);
                Conversation conv = simulator.GetSimulatedConversation(15);
                foreach(var m in conv.GetMessages())
                {
                    
                    if(outputChannel == null || outputChannelName == "source")
                    {
                        await ctx.RespondAsync(m.author + ": " + m.content);
                        await  Task.Delay(rnd.Next(2000, 5000));
                    }
                    else
                    {
                        await ctx.Client.SendMessageAsync(outputChannel, m.author + ": " + m.content);
                        await Task.Delay(rnd.Next(5000, 180000));
                    }
                    
                    
                }
            }
            else
            {
                Console.WriteLine("No simulated conversation found.");
            }

            
        }

        private async Task ExportMessagesForGuild(DiscordGuild g)
        {
            JArray output = new JArray();

            foreach (var c in g.Channels)
            {
                if (c.Value.Type == DSharpPlus.ChannelType.Text)
                {
                    var lastMessageId = c.Value.LastMessageId;
                    var done = false;
                    //List<string> output = new List<string>();

                    while (!done)
                    {
                        var response = await c.Value.GetMessagesAfterAsync(lastMessageId, 100);

                        if (response.Count <= 0)
                        {
                            done = true;
                        }
                        foreach (var r in response)
                        {
                            lastMessageId = Math.Min(lastMessageId, r.Id);
                            JObject json = JObject.FromObject(r);
                            json.Add("channel_name", c.Value.Name);
                            output.Add(json);
                        }
                    }
                }

            }
            
            Console.Out.WriteLine(output);
            SaveData(g.Name + "_messages.json", output);
        }

        void SaveData(string filename, string[] lines)
        {

            System.IO.File.WriteAllLines(@"E:\dev\triviabot\TriviaBot\Plugins\Admin\output\" + filename, lines);
        }

        void SaveData(string filename, JArray json)
        {
            System.IO.File.WriteAllText(@"E:\dev\triviabot\TriviaBot\Plugins\Admin\output\" + filename, json.ToString());
        }
    }
}
