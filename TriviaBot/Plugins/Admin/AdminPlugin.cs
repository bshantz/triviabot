﻿using System;
using System.Collections.Generic;
using System.Text;
using TriviaBot.Plugins;

namespace TriviaBot.Admin
{
    class AdminPlugin : IBotPlugin
    {
        public string Name
        {
            get
            {
                return "Admin Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            context.GetCommandsNextExtension.RegisterCommands<AdminCommands>();
        }
    }
}
