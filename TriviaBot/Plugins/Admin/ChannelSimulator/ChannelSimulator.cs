﻿using System;
using System.Collections.Generic;
using System.Text;
using Markov;
using DSharpPlus;
using DSharpPlus.Entities;
using System.Threading.Tasks;

namespace TriviaBot.Plugins.Admin.ChannelSimulator
{
    class ChannelSimulator
    {
        private MarkovChain<string> userChain; // Chain of users to determine sequence of responses
        private Dictionary<string, MarkovChain<string>> userMessageChains; // Chain of words by user for generating text
        private DiscordChannel channel;
        private List<string> authors;

        public ChannelSimulator(DiscordChannel channel)
        {
            authors = new List<string>();
            userChain = new MarkovChain<string>(1);
            userMessageChains = new Dictionary<string, MarkovChain<string>>();

            this.channel = channel;
        }

        // Pull message history from channel and rebuild the chain models
        public async Task UpdateModels(string topic = null)
        {

            if(topic == null)
            {
                Console.WriteLine("No Topic Selected. Using all text.");
            }
            else
            {
                Console.WriteLine("Topic is ["+topic+"].");
            }
            
            var lastMessageId = channel.LastMessageId;
            var done = false;
            authors = new List<string>();
            
            while (!done)
            {
                var response = await channel.GetMessagesAfterAsync(lastMessageId, 100);
               
                if (response.Count <= 0)
                {
                    done = true;
                }
                foreach (var r in response)
                {
                    if(topic == null || ContainsTopic(r.Content, topic))
                    {
                        if (!userMessageChains.ContainsKey(r.Author.Username))
                            userMessageChains.Add(r.Author.Username, new MarkovChain<string>(1));

                        userMessageChains[r.Author.Username].Add(SplitText(CleanupText(r.Content)));
                        authors.Add(r.Author.Username);
                        
                    }
                    lastMessageId = Math.Min(lastMessageId, r.Id);
                }
            }
            //authors.Reverse();
            userChain.Add(authors);
        }

        private string CleanupText(string str)
        {
            //str = str.Replace(".", " .");
            //str = str.Replace("!", " !");
            //str = str.Replace("?", " ?");
            //str = str.Replace(",", " ,");

            return str;
        }

        private string[] SplitText(string str)
        {
            return str.Split(" ");
        }

        public Conversation GetSimulatedConversation(int num = 60)
        {
            Random rnd = new Random();
            Conversation conv = new Conversation();
            
            for (int i = 0; i < num; i++)
            {
                var rndUser = authors[rnd.Next(authors.Count)];
                var sentence = string.Join(" ", userMessageChains[rndUser].Chain(rnd));
                conv.Add(new Message() { author = rndUser, content = sentence });
            }

            return conv;
        }

        public Conversation GetSimulatedConversationWithTopic(string topic)
        {
            Random rnd = new Random();
            Conversation conv = new Conversation();

            for (int i = 0; i < 10; i++)
            {
                var rndUser = authors[rnd.Next(authors.Count)];
                var sentence = string.Join(" ", userMessageChains[rndUser].Chain(new string[] { topic }, rnd));
                conv.Add(new Message() { author = rndUser, content = sentence });
            }

            return conv;
        }

        private Boolean ContainsTopic(string content, string topic)
        {
            content = content.ToLower();
            topic = topic.ToLower();

            return content.Contains(topic);
        }

    }
}
