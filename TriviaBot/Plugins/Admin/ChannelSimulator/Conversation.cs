﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TriviaBot.Plugins.Admin.ChannelSimulator
{
    class Message
    {
        public string author;
        public string content;
    }

    class Conversation
    {
        private List<Message> messages;

        public Conversation()
        {
            messages = new List<Message>();
        }

        public void Add(Message m)
        {
            messages.Add(m);
        }

        public List<Message> GetMessages()
        {
            return messages;
        }
    }
}
