﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins.DadJoke.Models;
using TriviaBot.Plugins.DadJoke.Services;
using static DSharpPlus.Entities.DiscordEmbedBuilder;

namespace TriviaBot.Plugins.DadJoke
{
    class DadJokeCommands : BaseCommandModule
    {
        [Command("dadjoke")]
        public async Task DadJoke(CommandContext ctx)
        {           
            DadJokeModel joke = await DadJokeDataService.GetRandomDadJoke();
            if (joke != null)
                await ctx.RespondAsync(embed: joke.AsDiscordEmbed());
            else
                await NoJoke(ctx);
        }

        [Command("dadjoke")]
        public async Task DadJoke(CommandContext ctx, string term)
        {
            DadJokeModel joke = await DadJokeDataService.GetRandomDadJoke(term);
            if (joke != null)
                await ctx.RespondAsync(embed: joke.AsDiscordEmbed());
            else
                await NoJoke(ctx);
        }

        private async Task NoJoke(CommandContext ctx)
        {
            await ctx.RespondAsync("I couldn't find a joke for you. :(");
        }
    }
}
