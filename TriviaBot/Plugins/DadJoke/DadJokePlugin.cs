﻿using System;
using System.Threading.Tasks;
using TriviaBot.Plugins;

namespace TriviaBot.Plugins.DadJoke
{
    class DadJokePlugin : IBotPlugin
    {
        public string Name
        {
            get
            {
                return "Dad Joke Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            context.GetCommandsNextExtension.RegisterCommands<DadJokeCommands>();
        }
    }
}
