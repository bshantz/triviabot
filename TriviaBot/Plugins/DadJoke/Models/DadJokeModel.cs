﻿using DSharpPlus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using static DSharpPlus.Entities.DiscordEmbedBuilder;

namespace TriviaBot.Plugins.DadJoke.Models
{
    class DadJokeModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("joke")]
        public string Joke { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        public DiscordEmbed AsDiscordEmbed()
        {
            return new DiscordEmbedBuilder
            {
                Author = new EmbedAuthor
                {
                    Name = "Dad Joke",
                    IconUrl = "https://pbs.twimg.com/profile_images/554577845087764480/jxy5Z3kH_400x400.jpeg"
                },
                //Title = round.CurrentClue.Category.Title,
                Description = Joke,
                Color = DiscordColor.LightGray
            };
        }
    }
}
