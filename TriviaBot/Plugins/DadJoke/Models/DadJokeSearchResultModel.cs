﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TriviaBot.Plugins.DadJoke.Models
{
    class DadJokeSearchResultModel
    {
        private static readonly Random rnd = new Random();

        [JsonProperty("current_page")]
        public string CurrentPage { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("next_page")]
        public string NextPage { get; set; }

        [JsonProperty("previous_page")]
        public string PreviousPage { get; set; }

        [JsonProperty("search_term")]
        public string SearchTerm { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("total_jokes")]
        public string TotalJokes { get; set; }

        [JsonProperty("total_pages")]
        public string TotalPages { get; set; }

        [JsonProperty("results")]
        public List<DadJokeModel> Results { get; set; }

        public DadJokeModel GetRandomJoke()
        {
            if(Results == null || Results.Count == 0)
            {
                return null;
            }
            else
            {
                var index = rnd.Next(0, Results.Count - 1);
                return Results[index];
            }
        }
    }
}
