﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TriviaBot.Plugins.DadJoke.Models;

namespace TriviaBot.Plugins.DadJoke.Services
{
    class DadJokeDataService : DataService
    {
        private const string BASE_URL = "https://icanhazdadjoke.com";
        private static string GetUrl(string path) => BASE_URL + path;
        private static Dictionary<string, string> headers = new Dictionary<string, string>()
        {
            {"Accept", "application/json" }
        };

        public static async Task<DadJokeModel> GetRandomDadJoke()
        {
            string jsonString = await ApiRequest(GetUrl("/"), headers);
            return JsonConvert.DeserializeObject<DadJokeModel>(jsonString);
        }

        public static async Task<DadJokeModel> GetRandomDadJoke(string term)
        {
            try
            {
                string jsonString = await ApiRequest(GetUrl("/search?term=" + term), headers);
                DadJokeSearchResultModel jokes = JsonConvert.DeserializeObject<DadJokeSearchResultModel>(jsonString);
                return jokes.GetRandomJoke();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }
            
        }
    }
}
