﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TriviaBot.Plugins
{
    class DataService
    {
        protected static HttpClient client = new HttpClient();

        protected static async Task<string> ApiRequest(string url)
        {
            return await ApiRequest(url, new Dictionary<string, string>());
        }

        protected static async Task<string> ApiRequest(string url, Dictionary<string, string> headers)
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, url))
            {
                foreach(var header in headers)
                {
                    requestMessage.Headers.Add(header.Key, header.Value);
                }
                
                HttpResponseMessage response = await client.SendAsync(requestMessage);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                else
                {
                    Console.WriteLine(response.StatusCode);
                    throw new Exception("failed to get API data from: " + url);
                }
            }
        }
    }
}
