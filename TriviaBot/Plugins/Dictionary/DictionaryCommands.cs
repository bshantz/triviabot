﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Data;

namespace TriviaBot.Plugins.Dictionary
{
    class DictionaryCommands : BaseCommandModule
    {
        [Command("define")]
        public async Task DefineWord(CommandContext ctx, string wordId)
        {
            var results = await OxfordDictionaryService.Entries("en-us", wordId);
            await ctx.RespondAsync("Testing: " + results);
        }
    }
}
