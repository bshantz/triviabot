﻿using System;
using System.Collections.Generic;
using System.Text;
using TriviaBot.Data;

namespace TriviaBot.Plugins.Dictionary
{
    class DictionaryPlugin : IBotPlugin
    {
        private string AppID;
        private string AppKey;

        public DictionaryPlugin(string appID, string appKey)
        {
            this.AppID = appID;
            this.AppKey = appKey;
        }

        public string Name
        {
            get
            {
                return "Dictionary Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            // Set the data service App ID and App Key for subsequent api requests. 
            OxfordDictionaryService.setAppId(AppID);
            OxfordDictionaryService.setAppKey(AppKey);

            context.GetCommandsNextExtension.RegisterCommands<DictionaryCommands>();
        }
    }
}
