﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins;

namespace TriviaBot.Data
{
    class OxfordDictionaryService : DataService
    {
        private static string BASE_URL = "https://od-api.oxforddictionaries.com/api/v2";

        private static Dictionary<string, string> headers = new Dictionary<string, string>();

        public static void setAppId(string appId)
        {
            headers.Add("app_id", appId);
        }

        public static void setAppKey(string appKey)
        {
            headers.Add("app_key", appKey);
        }

        public static async Task<string> Entries(string lang, string wordId)
        {
            string url = BASE_URL + "/entries/" + lang + "/" + wordId.ToLower();
            var results = await ApiRequest(url, headers);
            return results;
        }

        public static void Lemmas(string lang, string wordId)
        {

        }

        public static void Search(string lang)
        {

        }

        public static void Thesaurus(string lang, string wordId)
        {

        }

        public static void Sentences(string lang, string wordId)
        {

        }
    }
}
