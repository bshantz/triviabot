﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TriviaBot.Plugins
{
    interface IBotPlugin
    {
        string Name { get; }
        void InitCommands(IBotPluginContext context);
    }
}
