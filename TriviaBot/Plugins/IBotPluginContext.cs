﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.Lavalink;
using DSharpPlus.VoiceNext;
using Microsoft.Extensions.Configuration;

namespace TriviaBot.Plugins
{
    interface IBotPluginContext
    {
        DiscordClient GetDiscordClient { get; }
        CommandsNextExtension GetCommandsNextExtension { get; }
        InteractivityExtension GetInteractivityExtension { get; }
        LavalinkNodeConnection GetLavalinkNodeConnection { get;  }
        IConfiguration GetConfiguration { get; }
    }
}
