﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SkiaSharp;
using TriviaBot.Plugins.Jeopardy.Models;

namespace TriviaBot.Plugins.Jeopardy.Formatters
{
    class JeopardyBoard
    {
        private SKBitmap boardBitmap;

        private SKTypeface titleFont = SKTypeface.FromFile("HelveticaCdBd.ttf");        

        int numCols = 4;
        int numRows = 6;
        int imgWidth = 800;
        int imgHeight = 600;

        public string FilePath { get; private set; }

        public JeopardyBoard(JRound round) 
        {

            try
            {

                boardBitmap = new SKBitmap(imgWidth,
                                        imgHeight);

                var canvas = new SKCanvas(boardBitmap);
                canvas.Clear(SKColors.Blue);

                string categoryAlias = "abcd";

                float cellWidth = imgWidth / numCols;
                float cellHeight = imgHeight / numRows;

                int categoryCount = 0;
                int i = 0;
                int j = 0;
                foreach (var item in round.QuestionData)
                {
                    j = 0;
                    float x = i * cellWidth;
                    float y = j * cellHeight;

                    AddCategoryTitle(canvas, x, y, cellWidth, cellHeight, String.Format("{0}", item[0].Category.Title));
                    AddCategoryAlias(canvas, x, y, String.Format("({0})", categoryAlias[categoryCount]));

                    foreach (var clue in item)
                    {
                        x = i * cellWidth;
                        y = (j + 1) * cellHeight;

                        if (!clue.IsComplete)
                        {
                            canvas = AddGridSquareWithText(canvas, x, y, cellWidth, cellHeight, String.Format("${0}", clue.Value));
                        }
                        else
                        {
                            canvas = AddGridSquareWithWinner(canvas, x, y, cellWidth, cellHeight, clue.AnsweredBy);
                        }
                        j++;
                    }

                    categoryCount++;
                    i++;
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
            } 
        }

        public class Line
        {
            public string Value { get; set; }
            public float Width { get; set; }
        }

        private SKCanvas AddGridSquareWithText(SKCanvas canvas, float x, float y, float w, float h, string text)
        {
            using (SKPaint gridPaint = new SKPaint { Color = SKColors.Black, Style = SKPaintStyle.Stroke, StrokeWidth = 3 })
            {
                canvas.DrawRect(x, y, w, h, gridPaint);
            }
            
            float padding = 40;
            float scaledTextSize = Math.Min(GetTextSize(text, w - padding, h - padding), h*0.75f);
            using (SKPaint txtPaint = new SKPaint { Color = SKColors.Yellow, TextSize = scaledTextSize, Typeface = titleFont, IsAntialias = true })
            {
                SKRect bounds = new SKRect();
                txtPaint.MeasureText(text, ref bounds);
                
                canvas.DrawText(text, x+(w/2) - bounds.Width/2, y+(h/2) + bounds.Height/2, txtPaint);
            }
     
            return canvas;
        }

        private SKCanvas AddGridSquareWithWinner(SKCanvas canvas, float x, float y, float w, float h, string text)
        {
            text = text ?? " "; // Set text as an empty string if null, so we can still render the grid square.
            using (SKPaint gridPaint = new SKPaint { Color = SKColors.Black, Style = SKPaintStyle.Stroke, StrokeWidth = 3 })
            {
                canvas.DrawRect(x, y, w, h, gridPaint);
            }

            float padding = 40;
            float scaledTextSize = Math.Min(GetTextSize(text, w - padding, h - padding), h * 0.75f);
            using (SKPaint txtPaint = new SKPaint { Color = SKColors.LightBlue.WithAlpha(0x75), TextSize = scaledTextSize, Typeface = titleFont, IsAntialias = true })
            {
                SKRect bounds = new SKRect();
                txtPaint.MeasureText(text, ref bounds);

                canvas.DrawText(text, x + (w / 2) - bounds.Width / 2, y + (h / 2) + bounds.Height / 2, txtPaint);
            }

            return canvas;
        }

        private SKCanvas AddCategoryTitle(SKCanvas canvas, float x, float y, float w, float h, string text)
        {
            float padding = 40;
            float scaledTextSize = 32.0f;

            using (SKPaint gridPaint = new SKPaint { Color = SKColors.Black, Style = SKPaintStyle.Stroke, StrokeWidth = 2 })
            {
                canvas.DrawRect(x, y, w, h, gridPaint);
            }

            // Find appropriate size based on largest word
            string[] words = SplitWords(text);
            foreach (string word in words)
            {
                // Get word size plus empty space measurement
                float tmpSize = GetTextSize(word + " ", w - padding, h - padding);
                scaledTextSize = Math.Min(tmpSize, scaledTextSize);
            }

            using (SKPaint txtPaint = new SKPaint { Color = SKColors.White, TextSize = scaledTextSize, Typeface = titleFont, IsAntialias = true })
            {
                List<Line> lines = new List<Line>();
                string currentString = " ";
                int i = 0;
                foreach (string word in words)
                {
                    i++;

                    var combinedWidth = txtPaint.MeasureText(currentString + " " + word);
                    var maxWidth = w - padding;

                    if (combinedWidth > maxWidth)
                    {
                        lines.Add(new Line() { Value = currentString, Width = txtPaint.MeasureText(currentString) });
                        currentString = word;
                    }
                    else if (combinedWidth <= maxWidth)
                    {
                        currentString = currentString + " " + word;
                    }

                    // Handle last word
                    if (words.Count() == i)
                    {
                        lines.Add(new Line() { Value = currentString, Width = txtPaint.MeasureText(currentString) });
                    }
                }

                float lineHeight = txtPaint.TextSize * 1.0f;
                float height = lines.Count() * lineHeight;
                var lineY = y + (h / 2) - (height / 2);

                foreach (var line in lines)
                {
                    lineY += lineHeight;
                    var lineX = x + (w / 2) - (line.Width / 2);
                    canvas.DrawText(line.Value, lineX, lineY, txtPaint);
                }
            }
            return canvas;
        }

        private SKCanvas AddCategoryAlias(SKCanvas canvas, float x, float y, string text)
        {
            using (SKPaint txtPaint = new SKPaint { Color = SKColors.White, TextSize = 20.0f, Typeface = titleFont, IsAntialias = true })
            {
                canvas.DrawText(text, x + 5, y + txtPaint.TextSize, txtPaint);
            }
            return canvas;
        }

        private String[] SplitWords(string title)
        {
            return title.Split(" ");
        }

        private float GetTextSize(string text, float w, float h) 
        {
            float textSize = 12.0f;
            using (SKPaint paint = new SKPaint { TextSize = textSize })
            {
                SKRect bounds = new SKRect();
                paint.MeasureText(text, ref bounds);
                return w / bounds.Width * textSize;
            }
        }

        public Stream GetStream()
        {
            using (var image = SKImage.FromBitmap(boardBitmap))
            {
                string filename = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + "_jeopardy.jpg";
                return image.Encode(SKEncodedImageFormat.Jpeg, 80).AsStream();
            }
           
        }
    }
}
