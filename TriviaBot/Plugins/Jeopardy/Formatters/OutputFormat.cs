﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TriviaBot.Plugins.Jeopardy.Models;
using static DSharpPlus.Entities.DiscordEmbedBuilder;

namespace TriviaBot.Plugins.Jeopardy.Formatters
{
    class OutputFormat
    {
        public static DiscordEmbed FormatQuestionData(JRound round)
        {
            string categoryAlias = "abcdefgh";

            string response = "";
            int categoryCount = 0;
            foreach (var item in round.QuestionData)
            {
                response += String.Format("**({0}) {1}**\n\t", categoryAlias[categoryCount], item[0].Category.Title);
                foreach (var clue in item)
                {
                    if(!clue.IsComplete)
                    {
                        response += String.Format("\t{0}", clue.Value);
                    }
                    else
                    {
                        response += String.Format("\t~~{0}~~", clue.Value);
                    }
                    
                }
                response += "\n\n";
                categoryCount++;
            }

            DiscordEmbed embed = new DiscordEmbedBuilder
            {
                Author = new EmbedAuthor
                {
                    Name = "Jeopardy",
                    IconUrl = "https://78.media.tumblr.com/90aca1d3cf6b20b61c18af73d9afeccb/tumblr_inline_nf1bej61Bl1sfpj8f.jpg"
                },
                Title = "The Board",
                Description = response,
                Color = DiscordColor.Blue,
                Footer = new EmbedFooter
                {
                    Text = round.ControllingPlayer.DiscordUser.Username + " briefly controls the board."
                }
            }.AddField("Round Scores:", String.Join("\n", JeopardyPlayerManager.GetRoundLeaders().Select(x=>x.GetName() + ": " + x.RoundScore).ToArray()));

            return embed;
        }

        public static DiscordEmbed FormatFinalScore()
        {
            DiscordEmbed embed = new DiscordEmbedBuilder
            {
                Author = new EmbedAuthor
                {
                    Name = "Jeopardy",
                    IconUrl = "https://78.media.tumblr.com/90aca1d3cf6b20b61c18af73d9afeccb/tumblr_inline_nf1bej61Bl1sfpj8f.jpg"
                },
                Title = "Final Scores",
                Description = String.Join("\n", JeopardyPlayerManager.GetRoundLeaders().Select(x => x.GetName() + ": " + x.RoundScore).ToArray()),
                Color = DiscordColor.Gold
            };

            return embed;
        }
    } 
}
