﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.VoiceNext;
using TriviaBot.Plugins.Jeopardy.Formatters;
using TriviaBot.Plugins.Jeopardy.Models;
using static DSharpPlus.Entities.DiscordEmbedBuilder;


namespace TriviaBot.Plugins.Jeopardy
{
    
    class JeopardyCommands : BaseCommandModule
    {
        // Create or joins a jeopardy channel, and returns the voice connection.
        public async Task<DiscordChannel> GetOrCreateVoiceChannel(CommandContext ctx)
        {
            var channels = await ctx.Guild.GetChannelsAsync();

            DiscordChannel jeopardyChannel = null;

            // Check if a channel named Jeopardy exists already, if it does, join it. Otherwise, create one to join.
            if (channels.Where(x => x.Type == DSharpPlus.ChannelType.Voice && x.Name == "Jeopardy").Count() > 0)
            {
                jeopardyChannel = channels.Where(x => x.Type == DSharpPlus.ChannelType.Voice && x.Name == "Jeopardy").First();
            }
            else
            {
                jeopardyChannel = await ctx.Guild.CreateChannelAsync("Jeopardy", DSharpPlus.ChannelType.Voice);
            }

            return jeopardyChannel;
        }
      
        
        [Command("jeopardy")]
        public async Task Jeopardy(CommandContext ctx)
        {
            ulong roundID = ctx.Channel.Id;
            JRound round = await JeopardyRoundManager.GetRound(roundID);
            JPlayer player = JeopardyPlayerManager.GetPlayer(ctx.User);
            round.SetControllingPlayer(player);
            
            await PrintBoard(ctx, round);
        }

        [Command("jeopardy_reset")]
        public async Task JeopardyReset(CommandContext ctx)
        {
            JeopardyPlayerManager.ClearLeaders();
            ulong roundID = ctx.Channel.Id;

            JeopardyRoundManager.RemoveRound(roundID);
            JRound round = await JeopardyRoundManager.GetRound(roundID);
            JPlayer player = JeopardyPlayerManager.GetPlayer(ctx.User);
            round.SetControllingPlayer(player);

            await PrintBoard(ctx, round);
        }

        [Command("j")]
        public async Task Jeopardy(CommandContext ctx, string categoryLetter=null, int value=-1)
        {
            ulong roundID = ctx.Channel.Id;
            JRound round = await JeopardyRoundManager.GetRound(roundID);
            if (round.ControlLocked && round.ControllingPlayer.DiscordUser.Id != ctx.Message.Author.Id)
            {
                await ctx.RespondAsync("Calm down " + ctx.Message.Author.Username + ", " + round.ControllingPlayer.GetName() + " still has control of the board.");
                return;
            }

            if(round.CurrentClue != null && !round.CurrentClue.IsComplete)
            {
                await ctx.RespondAsync("A question is already in progress.");
                return;
            }

            if (categoryLetter != null && categoryLetter.Length == 1)
            {
                int categoryIndex = "abcd".IndexOf(categoryLetter);
                if (categoryIndex > -1)
                {
                    if (round.CurrentClue == null || round.CurrentClue.IsComplete)
                    {
                        if(value > -1)
                        {
                            round.SelectClue(categoryIndex, value);
                        }
                        else
                        {
                            round.SelectClue(categoryIndex);
                        }
                    }
                }
            }
            else if(categoryLetter == null && value == -1)
            {
                //DO auto select
                round.SelectNextClue();
            }

            if (round.CurrentClue != null && !round.CurrentClue.IsComplete)
            {
                await JeopardyQuestion(ctx, round);
            }

        }

        //[Command("j")]
        public async Task JeopardyQuestion(CommandContext ctx, JRound round)
        {
            var roundID = ctx.Channel.Id;

            if (round.CurrentClue != null)
            {
                DiscordEmbed clueEmbed = new DiscordEmbedBuilder
                {
                    Author = new EmbedAuthor
                    {
                        Name = "Jeopardy",
                        IconUrl = "https://78.media.tumblr.com/90aca1d3cf6b20b61c18af73d9afeccb/tumblr_inline_nf1bej61Bl1sfpj8f.jpg"
                    },
                    Title = round.CurrentClue.Category.Title,
                    Description = round.CurrentClue.Question,
                    Color = DiscordColor.Blue
                };
                await ctx.RespondAsync(embed: clueEmbed);

                bool correct = false;
                var interactivity = ctx.Client.GetInteractivity();
                await interactivity.WaitForMessageAsync(xm =>
                {
                    if(round.CurrentClue.CheckGuess(xm.Content))
                    {
                        correct = true;
                        JPlayer winner = JeopardyPlayerManager.GetPlayer(xm.Author);
                        winner.AddToScore(round.CurrentClue.Value.Value);
                        round.SetControllingPlayer(winner);
                        round.CurrentClue.AnsweredBy = winner.GetName();
                        return true;
                    }
                    return false;
                }, TimeSpan.FromSeconds(15));

                round.CurrentClue.IsComplete = true;

                string response = "";
                if(correct)
                {
                    response += "That's correct! The answer is **"+round.CurrentClue.Answer+"**";
                }
                else
                {
                    response += "I'm sorry! The answer we were looking for was **" + round.CurrentClue.Answer + "**"; 
                }

                await ctx.RespondAsync(response);

                await Task.Delay(1500);

                if(round.IsRoundComplete())
                {
                    await ctx.RespondAsync(embed: OutputFormat.FormatFinalScore());
                    // cleanup Round and Players
                    
                    await Task.Delay(5000);
                    await JeopardyReset(ctx);
                }
                else
                {
                    await PrintBoard(ctx, round);
                }
            }
            else
            {
                await ctx.RespondAsync("Invalid Clue Request");
            }

            round.PrintRoundInfo();
        }

        public async Task PrintBoardOriginal(CommandContext ctx, JRound round)
        {
            if (JeopardyRoundManager.LastBoardMessage != null)
            {
                await JeopardyRoundManager.LastBoardMessage.DeleteAsync();
                JeopardyRoundManager.LastBoardMessage = null;
            }
            JeopardyRoundManager.LastBoardMessage = await ctx.RespondAsync(embed: OutputFormat.FormatQuestionData(round));
        }

        public async Task PrintBoard(CommandContext ctx, JRound round)
        {
            if (JeopardyRoundManager.LastBoardMessage != null)
            {
                await JeopardyRoundManager.LastBoardMessage.DeleteAsync();
                JeopardyRoundManager.LastBoardMessage = null;
            }

            JeopardyBoard board = new JeopardyBoard(round);

            DiscordEmbed embed = new DiscordEmbedBuilder
            {
                Author = new EmbedAuthor
                {
                    Name = "Jeopardy",
                    IconUrl = "https://78.media.tumblr.com/90aca1d3cf6b20b61c18af73d9afeccb/tumblr_inline_nf1bej61Bl1sfpj8f.jpg"
                },
                Title = round.ControllingPlayer.DiscordUser.Username + " briefly controls the board.",
                Color = DiscordColor.Blue
            }.AddField("Round Scores:", String.Join("\n", JeopardyPlayerManager.GetRoundLeaders().Select(x => x.GetName() + ": " + x.RoundScore).ToArray()));
                
            string filename = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + "_jeopardy.jpg";
            JeopardyRoundManager.LastBoardMessage = await ctx.RespondWithFileAsync(filename, board.GetStream(), embed: embed);

        }

        [Command("answer_test")]
        public async Task RunTests(CommandContext ctx)
        {
            var tests = new Tests.AnswerTests();
            await tests.Run(ctx);
        }

    }
}
