﻿using System;
using System.Collections.Generic;
using System.Text;
using TriviaBot.Plugins.Jeopardy.Models;
using DSharpPlus.Entities;
using System.Linq;

namespace TriviaBot.Plugins.Jeopardy
{
    class JeopardyPlayerManager
    {
        private static Dictionary<ulong, JPlayer> players = new Dictionary<ulong, JPlayer>();

        public static bool HasRound(ulong userID)
        {
            return players.TryGetValue(userID, out JPlayer value);
        }

        public static JPlayer GetPlayer(DiscordUser user)
        {
            if (players.TryGetValue(user.Id, out JPlayer value))
            {
                return players.GetValueOrDefault(user.Id, null);
            }
            else
            {
                return CreatePlayer(user);
            }
        }

        public static JPlayer CreatePlayer(DiscordUser user)
        {
            JPlayer player = new JPlayer(user);
            players.Add(user.Id, player);

            return player;
        }

        public static void RemovePlayer(DiscordUser user)
        {
            if (players.TryGetValue(user.Id, out JPlayer value))
            {
                players.Remove(user.Id);
            }
        }

        public static List<JPlayer> GetRoundLeaders()
        {
            List<JPlayer> leaders = new List<JPlayer>();
            foreach (var item in players)
            {
                leaders.Add(item.Value);
            }
            return leaders.OrderByDescending(x => x.RoundScore).ToList();
        }

        public static void ClearLeaders()
        {
            players.Clear();
        }
    }
}
