﻿using System;
using System.Threading.Tasks;
using TriviaBot.Plugins;
using TriviaBot.Plugins.Jeopardy.Services;

namespace TriviaBot.Plugins.Jeopardy
{
    class JeopardyPlugin : IBotPlugin
    {
        public string Name
        {
            get
            {
                return "Jeopardy Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            InitAsync(context).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private async Task InitAsync(IBotPluginContext context)
        {
            await JService.LoadCategories();
            context.GetCommandsNextExtension.RegisterCommands<JeopardyCommands>();
        }
    }
}
