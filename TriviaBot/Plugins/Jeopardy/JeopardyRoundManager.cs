﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins.Jeopardy.Models;
using TriviaBot.Plugins.Jeopardy.Services;

namespace TriviaBot.Plugins.Jeopardy
{
    class JeopardyRoundManager
    {
        private static int numCategories = 4;
        private static Dictionary<ulong, JRound> activeRounds = new Dictionary<ulong, JRound>();
        public static DiscordMessage LastBoardMessage { get; set; }

        public static bool HasRound(ulong roundID)
        {
            return activeRounds.TryGetValue(roundID, out JRound value);
        }

        public static async Task<JRound> GetRound(ulong roundID)
        {
            if (activeRounds.TryGetValue(roundID, out JRound value))
            {  
                return activeRounds.GetValueOrDefault(roundID, null);
            }
            else
            {
                return await CreateRound(roundID);
            }
        }

        public static async Task<JRound> CreateRound(ulong gameID)
        {
            List<JCategory> categories = await JService.GetRandomCategories(numCategories);
            List<List<JClue>> questionData = new List<List<JClue>>();

            foreach (JCategory cat in categories) 
            {
                questionData.Add(await JService.GetClues(cat));    
            }

            JRound round = new JRound(questionData);
            activeRounds.Add(gameID, round);

            return round;
        }

        public static void RemoveRound(ulong roundID)
        {
            if (activeRounds.TryGetValue(roundID, out JRound value))
            {
                activeRounds.Remove(roundID);
            }
        }
    }
}
