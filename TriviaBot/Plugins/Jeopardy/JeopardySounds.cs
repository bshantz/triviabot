﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaBot.Plugins.Jeopardy
{
    class JeopardySounds
    {
        private static string DIR = "";

        public static string RIGHT_ANSEWR = DIR + "correct_answer.mp3";
        //public static string TIMES_UP = DIR + "times_up.mp3";
        //public static string BOARD_FILL = DIR + "jeopardy_categories.mp3";
        public static string THEME_SHORT = DIR + "jeopardy_theme_short.mp3";
        public static string THEME_LONG = DIR + "jeopardy_theme_long.mp3";
        public static string INTRO = DIR + "jeopardy_intro.mp3";
        public static string FAILURE = DIR + "new_low.mp3";
        public static string TIME_TO_ANSWER = DIR + "time_to_answer.mp3";
        public static string DAILY_DOUBLE = DIR + "daily_double.mp3";

        public static string TIMES_UP = "https://www.youtube.com/watch?v=dSmUtWKW7EA";// youtube URL
        public static string BOARD_FILL = "https://www.youtube.com/watch?v=TccJZfIGYSA";

    }

    class JeopardySoundManager
    {
        public static LavalinkNodeConnection LavaNode { get; private set; }
        public static void SetLavaNode(LavalinkNodeConnection node)
        {
            LavaNode = node;
        }

        private static Dictionary<ulong, LavalinkGuildConnection> guildConnections = new Dictionary<ulong, LavalinkGuildConnection>();

        public static LavalinkGuildConnection AddGuildConnection(ulong roundID, LavalinkGuildConnection lava)
        {
            guildConnections.Add(roundID, lava);
            return lava;
        }

        public static LavalinkGuildConnection GetGuildConnection(ulong roundID)
        {
            return guildConnections.GetValueOrDefault(roundID, null);
        }

        public static async Task PlaySound(ulong roundID, string sound)
        {
            CheckNode();

            var gc = GetGuildConnection(roundID);
            if(gc != null)
            {
                if(sound.StartsWith("http"))
                {
                    var track = await GetYoutubeTrack(sound);
                    gc.Play(track);
                }
                else
                {
                    var track = await GetLocalLavaLinkTrack(sound);
                    gc.Play(track);
                }
                
            }
        }

        public static void StopSounds(ulong roundID)
        {
            var gc = GetGuildConnection(roundID);
            if(gc != null)
            {
                gc.Stop();
            }
        } 

        public static async Task<LavalinkGuildConnection> CreateGuildConnection(CommandContext ctx, DiscordChannel voiceChannel)
        {
            if (voiceChannel.Type != DSharpPlus.ChannelType.Voice)
            {
                await ctx.RespondAsync("Cannot connect sounds to non-voice channel.");
                return null;
            }
            try
            {
                return await LavaNode.ConnectAsync(voiceChannel);
            }
            catch (Exception e)
            {
                await ctx.RespondAsync(e.Message);
            }
            return null;
        }

        private static void CheckNode()
        {
            if(LavaNode == null)
            {
                throw new Exception("lavaNode must be set before executing commands on SoundMangager.");
            }
        }

        private static async Task<LavalinkTrack> GetLocalLavaLinkTrack(string filename)
        {
            var fileInfo = new Uri(filename, UriKind.Relative);
            var trackResults = await LavaNode.GetTracksAsync(fileInfo);
            return trackResults.Tracks.First();
        }

        private static async Task<LavalinkTrack> GetYoutubeTrack(string url)
        {
            var trackResults = await LavaNode.GetTracksAsync(new Uri(url, UriKind.Absolute));
            return trackResults.Tracks.First();
        }
    }
}
