﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TriviaBot.Plugins.Jeopardy.Models
{
    class JCategory
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("clues_count")]
        public int CluesCount { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}
