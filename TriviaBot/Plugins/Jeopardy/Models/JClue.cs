﻿using Newtonsoft.Json;
using System;

namespace TriviaBot.Plugins.Jeopardy.Models
{
    class JClue
    {
        private static string[] wordsToIgnore = new string[] { "a", "an", "the" };

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("answer")]
        public string Answer { get; set; }

        [JsonProperty("question")]
        public string Question { get; set; }

        [JsonProperty("value")]
        public int? Value { get; set; }

        [JsonProperty("airdate")]
        public DateTime Airdate { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("category_id")]
        public int CategoryId { get; set; }

        [JsonProperty("game_id")]
        public object GameId { get; set; }

        [JsonProperty("invalid_count")]
        public int? InvalidCount { get; set; }

        [JsonProperty("category")]
        public JCategory Category { get; set; }

        public bool IsComplete { get; set; }

        public String AnsweredBy { get; set; }

        public bool CheckGuess(string guess)
        {
            JComparableAnswer finalGuess = new JComparableAnswer(guess);
            JComparableAnswer finalAnswer = new JComparableAnswer(Answer);
            
            if (finalGuess.Equals(finalAnswer))
            {
                return true;
            }
            else if(IsNumeric(finalAnswer.ToString())) 
            {
                return false;
            }
            else
            {
                float distance = finalAnswer.GetLevenshteinDistance(finalGuess);
                var answerLength = finalAnswer.ToString().Length;
                // if 85% of characters are correct, give it to them! 
                return ((answerLength - distance) / answerLength > 0.85f);
            }
        }

        private static bool IsNumeric(string str)
        {
            return decimal.TryParse(str, out decimal num);
        }
    }
}
