﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TriviaBot.Plugins.Utils;

namespace TriviaBot.Plugins.Jeopardy.Models
{
    class JComparableAnswer
    {
        private string answerString = null;

        private static List<string> ignoredWords = new List<string> { "a", "an", "the", "and", "it", "its" };

        private static string RemovePunctuation(string str)
        {
            return new string(str.ToCharArray().Where(c => !char.IsPunctuation(c)).ToArray());
        }

        private static string RemoveIgnoredWords(string str)
        {
            // Filter out the ignored words
            return String.Join(" ", str.Split(" ").Where(x => !ignoredWords.Contains(x)));
        }

        private static string RemoveWhitespace(string str)
        {
            return str.Replace(" ", "");
        }

        private static string RemoveHTML(string str)
        {
            return Regex.Replace(str, "<[a-zA-Z/].*?>", String.Empty);
        }

        // Some questions contain context in parentheses, for example, an answer might be "to pass (an automobile)". Context like this is difficult to match. 
        // So for our purposes, we are going to remove the sections enclosed in parentheses. 
        private static string RemoveParentheses(string str)
        {
            if(str.Contains("(") && str.Contains(")"))
            {
                var startIndex = str.IndexOf("(");
                var endIndex = str.LastIndexOf(")");
              
                return str.Remove(startIndex, endIndex - startIndex + 1).Trim();
            }
            else
            {
                return str;
            }
        }

        public JComparableAnswer(string str)
        {
            answerString = str.ToLower();
            answerString = RemoveHTML(answerString);
            answerString = RemoveParentheses(answerString);
            answerString = RemovePunctuation(answerString);
            answerString = RemoveIgnoredWords(answerString);
            answerString = RemoveWhitespace(answerString);
            // remove trailing S
            if(answerString.EndsWith('s'))
            {
                answerString = answerString.TrimEnd('s');
            }
        }

        public bool Equals(JComparableAnswer other)
        {
            return answerString == other.answerString;
        }

        public int GetLevenshteinDistance(JComparableAnswer other)
        {
            return LevenshteinDistance.Calculate(answerString, other.answerString);
        }

        public override string ToString()
        {
            return answerString;
        }
    }
}
