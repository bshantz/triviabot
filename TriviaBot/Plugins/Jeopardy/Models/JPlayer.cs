﻿using System;
using System.Collections.Generic;
using System.Text;
using DSharpPlus.Entities;

namespace TriviaBot.Plugins.Jeopardy.Models
{
    class JPlayer
    {
        public int AllTimeScore { get; private set; }
        public int RoundScore { get; private set; }
        public ulong Id { get; private set; }
        public DiscordUser DiscordUser { get; private set; }

        
        public JPlayer(DiscordUser discordUser)
        {
            DiscordUser = discordUser;

            AllTimeScore = 0;
            RoundScore = 0;
        }

        public void AddToScore(int score)
        {
            RoundScore += score;
            AllTimeScore += score;
        }

        public void RemoveFromScore(int score)
        {
            RoundScore -= score;
            AllTimeScore -= score;
        }

        public void ResetRoundScore()
        {
            RoundScore = 0;
        }
           
        public string GetName()
        {
            return DiscordUser.Username;
        }

        public ulong GetID()
        {
            return DiscordUser.Id;
        }
    }
}
