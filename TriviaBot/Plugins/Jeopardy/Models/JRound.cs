﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaBot.Plugins.Jeopardy.Models
{
    class JRound
    {
        public const int CONTROL_TIME_SECS = 5;

        public JClue CurrentClue { get; private set; }
        public List<List<JClue>> QuestionData { get; private set; }
        public JPlayer ControllingPlayer { get; private set; }
        public bool ControlLocked = false;
        public int LastCategoryIndex { get; private set; }

        public void SelectClue(int categoryIndex, int value) 
        {
            LastCategoryIndex = categoryIndex;
            CurrentClue = GetClue(categoryIndex, value);    
        }

        public void SelectClue(int categoryIndex)
        {
            LastCategoryIndex = categoryIndex;
            CurrentClue = GetClue(categoryIndex);
        }

        public JRound(List<List<JClue>> questionData)
        {
            this.LastCategoryIndex = 0; // Set 0 as default category index
            this.QuestionData = questionData;
        }

        public JClue GetClue(int categoryIndex, int value)
        {
            return this.QuestionData[categoryIndex].Find(x => x.Value == value);
        }

        public JClue GetClue(int categoryIndex)
        {
            var clues = this.QuestionData[categoryIndex].Where(c => !c.IsComplete);
            if(clues.Count() > 0)
            {
                return clues.First();
            }
            else
            {
                return null;
            }
        }

        public int CluesRemaining()
        {
            int remaining = QuestionData.Select(x => x.Where(c => !c.IsComplete).Count()).Sum();
            return remaining;
        }

        public async void SetControllingPlayer(JPlayer player)
        {
            ControllingPlayer = player;

            ControlLocked = true;
            await Task.Delay(CONTROL_TIME_SECS * 1000);
            ControlLocked = false;
        }

        public void SetCategoryIndex(int index)
        {
            LastCategoryIndex = index;
        }

        public void SelectNextClue()
        {
            var clues = this.QuestionData[LastCategoryIndex].Where(c => !c.IsComplete);
            if (clues.Count() > 0)
            {
                CurrentClue = clues.First();
            }
            else
            {
                CurrentClue = QuestionData.SelectMany(x => x.Where(c => !c.IsComplete)).First();
            }
            
        }

        public bool IsRoundComplete()
        {
            return CluesRemaining() == 0;
        }

        public void PrintRoundInfo()
        {
            Console.Out.WriteLine($"Clues Remaining: {CluesRemaining()}");
        }
    }
}
