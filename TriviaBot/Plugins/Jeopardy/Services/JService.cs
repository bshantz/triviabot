﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins.Jeopardy.Models;

namespace TriviaBot.Plugins.Jeopardy.Services
{
    class JService : DataService
    {
        private static Random rnd = new Random();
        private const int DEFAULT_CATEGORIES_PER_ROUND = 5;

        private static bool IsLoading = false;
        private static int MaxCategoryCacheCount = 10000;
        private static List<JCategory> categoryCache = new List<JCategory>();
        private static List<JCategory> completedCategories = new List<JCategory>();

        private const string BASE_URL = "http://jservice.io";

        private static string GetUrl(string path) => BASE_URL + path;

        public static async Task LoadCategories()
        {
            IsLoading = true;
            int currentOffset = 1;
            bool finished = false;
            while (!finished)
            {
                string jsonString = await ApiRequest(GetUrl("/api/categories?count=100&offset=" + currentOffset*100));
                List<JCategory> categories = JsonConvert.DeserializeObject<List<JCategory>>(jsonString);
                currentOffset++;
                if (categories.Count == 0)
                {
                    finished = true;
                    IsLoading = false;
                }
                else
                {
                    categoryCache.AddRange(categories);
                    if (categoryCache.Count >= MaxCategoryCacheCount)
                    {
                        finished = true;
                        IsLoading = false;
                    }
                }
            }
           
        }

        public static async Task<List<JCategory>> GetRandomCategories(int numCategories=DEFAULT_CATEGORIES_PER_ROUND)
        {
            if(!IsLoading && (categoryCache == null || categoryCache.Count == 0))
            {
                await LoadCategories();
            }

            List<JCategory> selectedCategories = new List<JCategory>();
            while(categoryCache.Count > numCategories - selectedCategories.Count && selectedCategories.Count < numCategories)
            {
                int rndIndex = rnd.Next(0, categoryCache.Count - 1);
                JCategory rndCategory = categoryCache[rndIndex];
                categoryCache.RemoveAt(rndIndex);
                selectedCategories.Add(rndCategory);
            }

            return selectedCategories;
        }

        public static async Task<List<JClue>> GetClues(int categoryId)
        {
            string jsonString = await ApiRequest(GetUrl("/api/clues?category=" + categoryId));
            return JsonConvert.DeserializeObject<List<JClue>>(jsonString);
        }

        public static async Task<List<JClue>> GetClues(JCategory category, int numClues=5)
        {
            string jsonString = await ApiRequest(GetUrl("/api/clues?category=" + category.Id));
            List<JClue> clues = JsonConvert.DeserializeObject<List<JClue>>(jsonString)
                .Where(x => (x.Answer != null & x.Question != null & x.Answer.Trim() != "" & x.Question.Trim() != ""))
                .GroupBy(x=>x.Answer, (key, g) => g.OrderBy(y=>y.UpdatedAt).First())
                .OrderBy(x => x.Value)
                .ToList();

            for (var i=0; i<clues.Count(); i++)
            {
                clues[i].Value = (i + 1) * 200;
            }

            return clues.GetRange(0, Math.Min(numClues, clues.Count() - 1));
        }

       
    }
}
