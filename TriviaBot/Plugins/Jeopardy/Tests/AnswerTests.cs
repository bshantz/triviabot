﻿using DSharpPlus.CommandsNext;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins.Jeopardy.Models;

namespace TriviaBot.Plugins.Jeopardy.Tests
{
    class AnswerTests
    {
        private static Dictionary<string, List<string>> testDict;

        public AnswerTests()
        {
            testDict = new Dictionary<string, List<string>>();

            AddTest("Brian Shantz", new List<string>() { "Brian", "Shantz", "brian Shantz", "BRIAN SHANTZ!"});
            AddTest("<i>italic test</i>", "italic test");
            AddTest("<em>emphasis test</em>", "emphasis test");
            AddTest("!punctuation!?test.,", "punctuation test");
            AddTest("plural tests", "plural test");
            AddTest("answer (and context)", "answer");
            // Relevant 
            // https://www.youtube.com/watch?v=EShUeudtaFg
            AddTest("pregnant", new List<string> {"pregnant", "pargnant", "pragnat", "prgnent", "peragnent", "preggo", "pregnnant", "pregmancy", "pregency", "oragnent", "pragment"});
        }

        public void AddTest(string answer, string guess)
        {
            CreateKeyIfNotExists(answer);
            testDict[answer].Add(guess);
        }

        public void AddTest(string answer, List<string> guesses)
        {
            CreateKeyIfNotExists(answer);
            testDict[answer].AddRange(guesses);
        }

        private void CreateKeyIfNotExists(string answer)
        {
            if(!testDict.ContainsKey(answer))
            {
                testDict[answer] = new List<string>();
            }
        }

        public void Run()
        {
            foreach(var answer in testDict.Keys)
            {
                var guesses = testDict[answer];
                foreach(var guess in guesses)
                {
                    var normalizedGuess = new JComparableAnswer(guess);
                    var normalizedAnswer = new JComparableAnswer(answer);
                    var result = normalizedGuess.Equals(normalizedAnswer);
                    Console.WriteLine(String.Format("{0} {1} {2}", guess, (result ? "==" : "!="), answer));
                    Console.WriteLine(String.Format("{0} {1} {2}", normalizedGuess, (result ? "==" : "!="), normalizedAnswer));
                }
            }
        }

        public async Task Run(CommandContext ctx)
        {
            string response = "Results: \n\n";

            foreach (var answer in testDict.Keys)
            {
                var guesses = testDict[answer];
                foreach (var guess in guesses)
                {
                    var normalizedGuess = new JComparableAnswer(guess);
                    var normalizedAnswer = new JComparableAnswer(answer);
                    var result = normalizedGuess.Equals(normalizedAnswer);
                    response += String.Format("{0} {1} {2}", guess, (result ? "==" : "!="), answer);
                    response += " -----> ";
                    response += String.Format("{0} {1} {2}", normalizedGuess, (result ? "==" : "!="), normalizedAnswer);
                    response += " --> Distnace: " + Utils.LevenshteinDistance.Calculate(normalizedGuess.ToString(), normalizedAnswer.ToString());
                    response += "\n";
                }
            }

            await ctx.RespondAsync(response);
        }
    }
}
