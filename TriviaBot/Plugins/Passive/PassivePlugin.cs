﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Vision.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace TriviaBot.Plugins.Passive
{
    class PassivePlugin : IBotPlugin
    {
        public string Name
        {
            get
            {
                return "Passive Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            var credential = GoogleCredential.FromFile("E:/dev/triviabot/brianshantz-fe511e9fa80f.json");
            // Instantiates a client
            var client = ImageAnnotatorClient.Create();

            context.GetDiscordClient.MessageCreated += async e =>
            {
                if(IsImage(e.Message.Content))
                {

                    // Load the image file into memory
                    var image = Image.FromUri(e.Message.Content);
                    
                    try
                    {
                        // Performs label detection on the image file
                        //var response = client.DetectLabels(image);
                        var safeSearch = client.DetectSafeSearch(image);
                        var msg = "";

                        await e.Message.RespondAsync("How likely is this to be Adult Content? " + safeSearch.Adult);
                        await e.Message.RespondAsync("How likely is this to be Racy Content? " + safeSearch.Racy);
                        await e.Message.RespondAsync("How likely is this to be Violent Content? " + safeSearch.Violence);
                        await e.Message.RespondAsync("How likely is this to be Medical Content? " + safeSearch.Medical);
                        /*
                        foreach (var annotation in response)
                        {
                            if (annotation.Description != null)
                                msg += annotation.Description + "\n";
                        }
                        if(msg != "")
                        {
                            await e.Message.RespondAsync("Looks like we have an image of: \n" + msg);
                        }
                        */
                    }
                    catch(Exception err)
                    {
                        Console.WriteLine(err.Message);
                    }
                    
                }
            };
        }

        private Boolean IsImage(string str)
        {
            str = str.ToLower();
            return (str.StartsWith("http") && (str.Contains(".jpg") || str.Contains(".png") || str.Contains(".gif") || str.Contains(".jpeg")));
        }
    }
}
