﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins.RedditPlugin.Services;
using static DSharpPlus.Entities.DiscordEmbedBuilder;

namespace TriviaBot.Plugins.RedditPlugin
{
    class RedditCommands : BaseCommandModule
    {
        private static Random rng = new Random();
        
        [Command("ff"), Aliases("mjbme")]
        public async Task RandomPost(CommandContext ctx)
        {
            if(ctx.Channel.IsNSFW)
            {
                int nsfwProb = 5;
                int happyProb = 5;
                int total = nsfwProb + happyProb;
                int random = rng.Next(0, total + 1);

                var happyPost = RedditDataService.GetRandomHappyPost();
                var nsfwPost = RedditDataService.GetRandomNSFWPost();
                var selectedPost = (random <= nsfwProb) ? nsfwPost : happyPost;

                if(selectedPost != null)
                {
                    DiscordEmbed embed = new DiscordEmbedBuilder
                    {
                        Author = new EmbedAuthor
                        {
                            Name = "50/50",
                            IconUrl = "https://www.pngkey.com/png/detail/229-2291389_smiley-face-evil-grin-mischievous-smiley-face.png"
                        },
                        Title = selectedPost.Title,
                        Description = String.Format("Courtesy of [/r/{0}]\n {1} \n {2}", selectedPost.Listing.Subreddit, selectedPost.Listing.URL, "https://reddit.com" + selectedPost.Listing.Permalink),
                        Color = DiscordColor.DarkGray,
                        ImageUrl = selectedPost.Listing.URL
                    };
                    await ctx.RespondAsync(embed: embed);
                }
                else
                {
                    await ctx.RespondAsync("Sorry, I couldn't find anything.");
                }
                
            }
            else
            {
                await ctx.RespondAsync("I'm sorry, it is not safe to run this command in this discord channel. Try a NSFW channel.");
            }
        }

        [Command("reddit_refresh")]
        public void RefreshPosts(CommandContext ctx)
        {
            RedditDataService.RefreshPosts();
        }
    }
}
