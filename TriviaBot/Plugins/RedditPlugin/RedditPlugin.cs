﻿using System;
using System.Collections.Generic;
using System.Text;
using TriviaBot.Plugins.RedditPlugin.Services;

namespace TriviaBot.Plugins.RedditPlugin
{
    class RedditPlugin : IBotPlugin
    {
        public string Name 
        {
            get
            {
                return "Reddit Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            context.GetCommandsNextExtension.RegisterCommands<RedditCommands>();

            // Init Reddit Service
            var appId = context.GetConfiguration["redditAppId"];
            var appSecret = context.GetConfiguration["redditAppSecret"];
            var refreshToken = context.GetConfiguration["redditRefreshToken"];
            var accessToken = context.GetConfiguration["redditAccessToken"];
            RedditDataService.Init(appId, appSecret, refreshToken, accessToken);
        }
    }
}
