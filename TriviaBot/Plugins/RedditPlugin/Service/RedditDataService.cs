using Reddit;
using Reddit.Controllers;
using Reddit.Inputs.Search;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TriviaBot.Plugins.RedditPlugin.Services
{
    class RedditDataService : DataService
    {
        private static RedditAPI reddit;
        private static Random rng = new Random();
        private static List<string> happySubreddits = new List<string>() {
            "eyebleach",
            "aww", 
            "bassethounds",
            "Boop",
            "sneks",
            "pigtures",
            "AnimalsBeingDerps",
            "funny"
        };

        // List provided by @MJB
        private static List<string> nsfwSubreddits = new List<string>() {
            "nsfw",
            "nsfw_gifs",
            "realgirls",
            "GirlswithNeonHair",
            "feet",
            "MasterOfAnal",
            "postorgasm",
            "rimming",
            "RuinedOrgasm",
            "VerticalGifs",
            "trashyboners",
            "WomenOfColor",
            "alteredbuttholes",
            "ass2mouth",
            "celebnsfw",
            "blowjob",
            "Break_Yo_Dick_Thick",
            "creampie",
            "MorbidReality",
            "BadDragon",
            "doublepenetration",
            "FilthyGirls",
            "GodAsshole",
            "rule34",
            "NSFW_ANALQUEENS",
            "hentai",
            "Tentai",
            "NSFW_Comics",
            "MedicalGore",
            "tightdresses",
            "Pegging",
            "dark_humor",
            "nsfw_wtf",
            "HairyPussy",
            "pronebone",
            "Pee",
            "HijabiXXX",
            "FurryPornSubreddit"
        };

        private static List<Post> nsfwPosts;
        private static List<Post> happyPosts;

        public static void Init(String appId, string appSecret, string refreshToken, string accessToken)
        {
            nsfwPosts = new List<Post>();
            happyPosts = new List<Post>();

            reddit = new RedditAPI(appId: appId, refreshToken: refreshToken, appSecret: appSecret, accessToken: accessToken);

            RefreshPosts();
            
        }

        public static void RefreshPosts()
        {
            nsfwPosts = GetPostsFromSubbreddits(nsfwSubreddits);
            happyPosts = GetPostsFromSubbreddits(happySubreddits);
        }

        public static List<Post> GetPostsFromSubbreddits(List<string> subreddits)
        {
            bool isValid(Post p)
            {
                return !IsVReddit(p);
            }

            var posts = new List<Post>();
            var after = "";
            foreach(var sub in subreddits)
            {
                var result = reddit.Subreddit(sub).Posts.GetHot(limit: 100, after: after).Where(IsImage).ToList();
                if(result.Count > 0)
                {
                    posts.AddRange(result);
                }
                Console.WriteLine("Posts: " + posts.Count);
            }
            return posts;
        }

        private static bool IsVReddit(Post p)
        {
            return p.Listing.URL.Contains("v.redd.it");
        }

        private static bool IsImage(Post p)
        {
            var url = p.Listing.URL;
            return url.EndsWith(".jpg") || url.EndsWith(".jpeg") || url.EndsWith(".gif") || url.EndsWith(".png");
        }

        public static Post GetRandomNSFWPost()
        {
            return GetRandomPost(ref nsfwPosts);
        }

        public static Post GetRandomHappyPost()
        {
            return GetRandomPost(ref happyPosts);
        }

        public static Post GetRandomPost(ref List<Post> posts)
        {
            if (posts.Count > 0)
            {
                var randomIndex = rng.Next(0, posts.Count);
                Post p = posts[randomIndex];
                posts.RemoveAt(randomIndex);
                return p;
            }
            else
            {
                return null;
            }
        }
       

        
    }
}