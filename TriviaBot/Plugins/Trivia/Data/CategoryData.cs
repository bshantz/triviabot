﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TriviaBot.Trivia.Data
{
    class TriviaCategory
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    class RootObject
    {
        public List<TriviaCategory> trivia_categories { get; set; }
    }

    class CategoryData : TriviaDataService
    {
        private static Random rnd = new Random();
        private static List<TriviaCategory> Categories;

        public static async Task<List<TriviaCategory>> GetCategories()
        {
            if(Categories == null)
            {
                string jsonString = await ApiRequest("https://opentdb.com/api_category.php");
                RootObject root = JsonConvert.DeserializeObject<RootObject>(jsonString);
                Categories = root.trivia_categories;
            }
            return Categories;
        }

        public static async Task<TriviaCategory> FindTriviaCategory(string search)
        {
            List<TriviaCategory> categories = await GetCategories();
            TriviaCategory category;
            
            // If the trivia category is "any", return a random category.
            if (search.ToLower() == "any")
            {
                int r = rnd.Next(categories.Count);
                return categories[r];
            }
            else
            {
                category = categories.Find(x => x.id.ToString() == search.Trim());
            }
            
            // If no category was found, try searching for a category substring
            if (category == null)
            {
                category = categories.Find(x => { return x.name.ToLower().Contains(search); });
            }
            
            return category;
        }

    }
}
