﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Plugins.Trivia.Data;
using TriviaBot.Trivia.Data;

namespace TriviaBot.Trivia
{

    public class Result
    {
        public string category { get; set; }
        public string type { get; set; }
        public string difficulty { get; set; }
        public string question { get; set; }
        public string correct_answer { get; set; }
        public List<string> incorrect_answers { get; set; }
    }

    public class RootObject
    {
        public int response_code { get; set; }
        public List<Result> results { get; set; }
    }

    class QuestionData : TriviaDataService
    {
        
        public static async Task<List<Question>> LoadQuestions(string category, int numQuestions, TriviaDbToken token=null)
        {

            string url = "https://opentdb.com/api.php?amount=" + numQuestions;
            if(category != "any")
            {
                url += "&category=" + category;
            }
            if(token != null)
            {
                url += "&token=" + token.Token;
            }
            string jsonString = await ApiRequest(url);
            
            RootObject root = JsonConvert.DeserializeObject<RootObject>(jsonString);
            List<Question> questions = new List<Question>();
            if (root.response_code == 4 || root.response_code == 1)
            {
                return questions;
            }
            root.results.ForEach(result =>
            {
                questions.Add(new Question
                (
                    WebUtility.HtmlDecode(result.category),
                    WebUtility.HtmlDecode(result.type),
                    WebUtility.HtmlDecode(result.difficulty),
                    WebUtility.HtmlDecode(result.question),
                    WebUtility.HtmlDecode(result.correct_answer),
                    result.incorrect_answers.Select(answer => WebUtility.HtmlDecode(answer)).ToList<string>()
                ));
            });

            return questions;
        }

    }
}
