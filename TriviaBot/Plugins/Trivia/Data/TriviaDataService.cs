﻿
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using TriviaBot.Plugins;
using TriviaBot.Plugins.Trivia.Data;

namespace TriviaBot.Trivia.Data
{
    class TriviaDataService : DataService
    {
        private static TriviaDbToken token = null;
        private static string BaseUrl = "https://opentdb.com";

        public static async Task ResetToken()
        {
            string jsonString = await ApiRequest(BaseUrl + "/api_token.php?command=request");
            token = JsonConvert.DeserializeObject<TriviaDbToken>(jsonString);
        }

        public static async Task<List<Question>> GetQuestions(string category, int numQuestions)
        {
            if(token == null)
            {
                await ResetToken();
            }
            return await QuestionData.LoadQuestions(category, numQuestions, token);
        }


    }
}
