﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TriviaBot.Plugins.Trivia.Data
{
    class TriviaDbToken
    {   
        [JsonProperty("response_code")]
        public int ResponseCode { get; set; }

        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
