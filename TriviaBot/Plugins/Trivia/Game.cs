﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaBot.Trivia
{
    class Game
    {
        public ulong AuthorId { get; set; }
        public long Id { get; private set; }

        public List<Question> questions { get; private set; }
        private int currentQuestionIndex;

        public Game(List<Question> questions)
        {
            this.questions = questions;
            this.currentQuestionIndex = -1;
            this.Id = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        }

        public bool HasMoreQuestions()
        {
            return currentQuestionIndex < questions.Count()-1;
        }

        public Question GetNextQuestion()
        {
           if(HasMoreQuestions())
            {
                currentQuestionIndex++;
                return questions[currentQuestionIndex];
            }
            else
            {
                return null;
            }
        }

        public int GetQuestionNumber()
        {
            return currentQuestionIndex;
        }

        private Question GetQuestion(int questionIndex)
        {
            if(questions[questionIndex] != null)
            {
                return questions[questionIndex];
            }
            else
            {
                return null;
            }
        }

        public string GetFormattedOptions()
        {
            Question question = questions[currentQuestionIndex];
            string text = "";
            for (var i = 0; i < question.GetIndexedOptions().Count(); i++)
            {
                text += $"\n {i}) {question.GetIndexedOptions()[i]}";
            }
            return text;
        }

        public string FormatQuestionAnswersForDiscord()
        {
            Question question = questions[currentQuestionIndex];

            string text = $"Times up! The correct answer is: **{question.correct_answer}**\n\n";
            List<string> correctUsers = new List<string>();
            List<string> incorrectUsers = new List<string>();

            foreach (KeyValuePair<string, PlayerResponse> entry in question.playerAnswers)
            {
                if(entry.Value.answer == question.correct_answer)
                {
                    correctUsers.Add(entry.Key);
                }
                else
                {
                    incorrectUsers.Add(entry.Key);
                }
            }

            if (correctUsers.Count() > 0)
            {
                text += "```diff\n" +
                    "+ " + ReplaceLastComma(String.Join(", ", correctUsers.ToArray()) + " answered correctly!\n\n") +
                    "```";

            }

            if(incorrectUsers.Count() > 0)
            {
                text += "```diff\n" +
                "- " + ReplaceLastComma(String.Join(", ", incorrectUsers.ToArray()) + " got it wrong.") +
                "```";
            }
            
            return text;
        }

        private static string ReplaceLastComma(string text, string replace=" and")
        {
            var lastComma = text.LastIndexOf(',');
            if (lastComma > -1 && lastComma < text.Length - 1)
            {
                text = text.Substring(0, lastComma)
                         + replace
                         + text.Substring(lastComma + 1);
            }
            return text;
        }
    }
}
