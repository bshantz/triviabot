﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TriviaBot.Trivia.Data;

namespace TriviaBot.Trivia
{
    class GameManager
    {
        private static Dictionary<ulong, Game> activeGames = new Dictionary<ulong, Game>();

        public static bool HasGame(ulong gameID)
        {
            return activeGames.TryGetValue(gameID, out Game value);
        }

        public static async Task<Game> GetOrCreateGame(ulong gameID, string category, int numQuestions = 1)
        {
            if(activeGames.TryGetValue(gameID, out Game value))
            {
                return activeGames.GetValueOrDefault(gameID, null);
            }
            else
            {
                List<Question> questions = await TriviaDataService.GetQuestions(category, numQuestions);
                if(questions.Count < numQuestions)
                {
                    throw new Exception("No questions remain for category [" + category + "]");
                }
                activeGames.Add(gameID, new Game(questions));
                return activeGames.GetValueOrDefault(gameID, null);
            }
        }

        public static void RemoveGame(ulong gameID)
        {
            if (activeGames.TryGetValue(gameID, out Game value))
            {
                activeGames.Remove(gameID);
            }
        }
    }
}
