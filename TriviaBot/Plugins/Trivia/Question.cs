﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TriviaBot.Trivia
{

    class PlayerResponse
    {
        public string answer { get; private set; }
        public int responseTime { get; private set; }

        public PlayerResponse(string answer, int time)
        {
            this.answer = answer;
            this.responseTime = time;
        }
    }

    class Question
    {
        private static Random rnd = new Random(DateTime.Now.Millisecond);
        public Dictionary<string, PlayerResponse> playerAnswers { get; private set; }
        public int correct_index { get; private set; }
        public List<string> options { get; private set; }
        public string category { get; private set; }
        public string type { get; private set; }
        public string difficulty { get; private set; }
        public string question { get; private set; }
        public string correct_answer { get; private set; }
        public List<string> incorrect_answers { get; private set; }
        public int Id { get; private set; }

        public Question(string category, string type, string difficulty, string question, string correct_answer, List<string> incorrect_answers)
        {
            playerAnswers = new Dictionary<string, PlayerResponse>();
            this.category = category;
            this.type = type;
            this.difficulty = difficulty;
            this.question = question;
            this.correct_answer = correct_answer;
            this.incorrect_answers = incorrect_answers;
            this.Id = question.GetHashCode();

            options = new List<string>();
            options.AddRange(incorrect_answers);
            int correctIndex = rnd.Next(0, options.Count());
            options.Insert(correctIndex, correct_answer);
        }

        public List<String> GetIndexedOptions()
        {
            return options;
        }

        public bool SubmitAnswer(string username, int optionIndex, int time=0)
        {
            if(optionIndex < 0 || optionIndex >= options.Count())
            {
                return false;
            }
            else if (!playerAnswers.ContainsKey(username) && options[optionIndex] != null) 
            {
                string playerAnswerText = options[optionIndex];
                playerAnswers.Add(username, new PlayerResponse(playerAnswerText, time));
                bool isCorrect = (correct_answer == playerAnswerText);
                return isCorrect;
            }
            return false;
        }

        public bool HasUserAnswered(string username)
        {
            return playerAnswers.ContainsKey(username);
        }

        public string GetAnswerForOptionIndex(int index)
        {
            return options[index];
        }
    }
}
