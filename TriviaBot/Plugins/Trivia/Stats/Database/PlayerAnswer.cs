﻿using System;
using System.Collections.Generic;

namespace TriviaBot.Trivia.Stats.Database
{
    public partial class PlayerAnswer
    {
        public long Id { get; set; }
        public int? QuestionId { get; set; }
        public long? GameId { get; set; }
        public long UserId { get; set; }
        public string Username { get; set; }
        public string Category { get; set; }
        public string QuestionType { get; set; }
        public string Difficulty { get; set; }
        public short IsCorrect { get; set; }
        public DateTime? EventTime { get; set; }
        public long? GameAuthorId { get; set; }
        public long? ResponseTimeMs { get; set; }
    }
}
