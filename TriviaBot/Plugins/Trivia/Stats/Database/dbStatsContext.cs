﻿using System;
using Microsoft.EntityFrameworkCore;

namespace TriviaBot.Trivia.Stats.Database
{
    public partial class DbStatsContext : DbContext
    {
        public virtual DbSet<PlayerAnswer> PlayerAnswer { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string host = Program.Configuration["pgHost"];
                string user = Program.Configuration["pgUser"];
                string pass = Program.Configuration["pgPass"];
                string databaseName = Program.Configuration["pgDatabase"];

                optionsBuilder.UseNpgsql(String.Format("Host={0};Database={1};Username={2};Password={3}", host, databaseName, user, pass));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlayerAnswer>(entity =>
            {
                entity.ToTable("player_answer", "triviabot");

                entity.HasIndex(e => e.Category)
                    .HasName("category_idx");

                entity.HasIndex(e => e.GameId)
                    .HasName("game_id_idx");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('triviabot.player_answer_id_seq'::regclass)");

                entity.Property(e => e.Category).HasColumnName("category");

                entity.Property(e => e.Difficulty).HasColumnName("difficulty");

                entity.Property(e => e.EventTime).HasColumnName("event_time");

                entity.Property(e => e.GameAuthorId).HasColumnName("game_author_id");

                entity.Property(e => e.GameId).HasColumnName("game_id");

                entity.Property(e => e.IsCorrect).HasColumnName("is_correct");

                entity.Property(e => e.QuestionId).HasColumnName("question_id");

                entity.Property(e => e.QuestionType).HasColumnName("question_type");

                entity.Property(e => e.ResponseTimeMs).HasColumnName("response_time_ms");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Username).HasColumnName("username");
            });

            modelBuilder.HasSequence("player_answer_id_seq");
        }
    }
}
