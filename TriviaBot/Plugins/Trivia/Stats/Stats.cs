﻿using System;
using System.Collections.Generic;
using System.Linq;
using TriviaBot.Trivia.Stats.Database;

namespace TriviaBot.Trivia.Stats
{
    public class LeaderBoardResult
    {
        public string Category { get; set; }
        public string Username { get; set; }
        public double AvgResponseTimeMs { get; set; }
        public double TotalScore { get; set; }
        public int TotalAnswers { get; set; }
        public int CorrectAnswers { get; set; }
    }


    public sealed class Stats
    {
        private static volatile Stats instance;
        private static object syncRoot = new Object();

        private Stats()
        {
        }

        public static Stats Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Stats();
                    }
                }

                return instance;
            }
        }

       
        public void InsertPlayerAnswer(PlayerAnswer playerAnswer)
        {
            using (var db = new DbStatsContext())
            {
                db.PlayerAnswer.Add(playerAnswer);
                db.SaveChanges();
            }
        }

        public List<LeaderBoardResult> GetPlayerStatsByCategory(string username)
        {
            using (var db = new DbStatsContext())
            {
                var results = db.PlayerAnswer.Where(o=>o.Username.ToLower() == username.ToLower()).GroupBy(o => o.Category)
                                .Select(g => new LeaderBoardResult {
                                    Category = g.Key,
                                    Username = username,
                                    CorrectAnswers = g.Sum(i => i.IsCorrect),
                                    TotalAnswers = g.Count(),
                                    AvgResponseTimeMs = (double)g.Average(i => i.ResponseTimeMs)
                                });
                return results.ToList();
            }
        }

        public List<LeaderBoardResult> GetPlayerStatsForGame(long gameID)
        {
            using (var db = new DbStatsContext())
            {
                var results = db.PlayerAnswer.Where(o=>o.GameId == gameID).GroupBy(o => o.Username)
                                .Select(g => new LeaderBoardResult {
                                    Username = g.Key,
                                    CorrectAnswers = g.Sum(i => i.IsCorrect),
                                    TotalAnswers = g.Count(),
                                    AvgResponseTimeMs = (double)g.Average(i => i.ResponseTimeMs)
                                });
                return results.ToList();
            }
        }

        public List<LeaderBoardResult> GetPlayerStatsTotal()
        {
            using (var db = new DbStatsContext())
            {
                var results = db.PlayerAnswer.GroupBy(o => o.Username)
                                .Select(g => new LeaderBoardResult {
                                    Username = g.Key,
                                    CorrectAnswers = g.Sum(i => i.IsCorrect),
                                    TotalAnswers = g.Count(),
                                    AvgResponseTimeMs = (double)g.Average(i => i.ResponseTimeMs)
                                });
                return results.ToList();
            }
        }

        public void DeletePlayerStats(ulong userId)
        {
            using (var db = new DbStatsContext())
            {
                db.PlayerAnswer.RemoveRange(db.PlayerAnswer.Where(x => (ulong)x.UserId == userId));
                db.SaveChanges();
            }
        }
    }
}
