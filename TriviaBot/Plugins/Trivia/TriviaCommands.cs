﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using TriviaBot.Trivia;
using TriviaBot.Trivia.Data;
using TriviaBot.Trivia.Stats;
using TriviaBot.Trivia.Stats.Database;
using static DSharpPlus.Entities.DiscordEmbedBuilder;
using static TriviaBot.Trivia.Data.CategoryData;

namespace TriviaBot
{
    public class TriviaCommands : BaseCommandModule
    {
        public event OnPlayerAnswer OnPlayerAnswer_Event;
        public delegate void OnPlayerAnswer(CommandContext ctx, DiscordMessage m, PlayerAnswer answerRecord);

        public TriviaCommands()
        {
            OnPlayerAnswer_Event += new OnPlayerAnswer(HidePlayerAnswer);
            OnPlayerAnswer_Event += new OnPlayerAnswer(HandlePlayerStats);
        }

        [Command("categories")]
        public async Task Categories(CommandContext ctx)
        {
            List<TriviaCategory> categories = await CategoryData.GetCategories();
            string response = $"**Available Categories are:**";
            categories.ForEach(cat =>
            {
                response += $"\n**{cat.id}** : {cat.name}";
            });
            await ctx.RespondAsync(response);
        }

        [Command("trivia")]
        public async Task Trivia(CommandContext ctx, string category="any", int questions=1)
        {
            TriviaCategory selectedCategory = await FindTriviaCategory(category);
            if(selectedCategory == null)
            {
                await ctx.RespondAsync($"Invalid Category ID [{category}]. Also, no category names contain the substring [{category}]");
                return;
            }
            
            // Load Data for new Game
            // Initialize game with questions from API if one isn't already initialized
            if(!GameManager.HasGame(ctx.Channel.Id))
            {
                try
                {
                    Game game = await GameManager.GetOrCreateGame(ctx.Channel.Id, selectedCategory.id.ToString(), Math.Min(Math.Abs(questions), 5));
                    game.AuthorId = ctx.User.Id;
                    SendQuestion(ctx, game);
                }
                catch(Exception e)
                {
                    await ctx.RespondAsync("Exception Thrown: " + e.Message);
                }
                
            }
        }

        [Command("trivia_reset_token")]
        public async Task TriviaResetToken(CommandContext ctx)
        {
            await TriviaDataService.ResetToken();
            await ctx.RespondAsync("The TriviaDb token has been reset.");
        }

        [Command("stats")]
        public async Task GetStats(CommandContext ctx, string username=null)
        {
            if(username == null) 
            {
                List<LeaderBoardResult> results = Stats.Instance.GetPlayerStatsTotal();
                DiscordEmbed leaderboardEmbed = new DiscordEmbedBuilder
                {
                    Author = new EmbedAuthor
                    {
                        Name = "All Time Results",
                        IconUrl = "http://www.pvhc.net/img25/kabobvfpbpjxllrhnqou.png"
                    },
                    Title = "Leaderboard:",
                    Description = String.Join("\n", results.OrderByDescending(x => (double)x.CorrectAnswers/(double)x.TotalAnswers).Select(x => x.Username + ": " + x.CorrectAnswers + " of " + x.TotalAnswers + " correct. (AVG Response Time of "+Math.Round(x.AvgResponseTimeMs/1000, 2)+" seconds)")),
                    Color = DiscordColor.Blue
                };
                await ctx.RespondAsync(embed: leaderboardEmbed);
            }
            else
            {
                List<LeaderBoardResult> results = Stats.Instance.GetPlayerStatsByCategory(username);
                DiscordEmbed leaderboardEmbed = new DiscordEmbedBuilder
                {
                    Author = new EmbedAuthor
                    {
                        Name = "All Time Results by Category ("+username+")",
                        IconUrl = "http://www.pvhc.net/img25/kabobvfpbpjxllrhnqou.png"
                    },
                    Title = "Overview:",
                    Description = String.Join("\n", results.OrderByDescending(x => (double)x.CorrectAnswers/(double)x.TotalAnswers)
                                        .Select(x => "**"+x.Category + ":**\n\t" + x.CorrectAnswers + " of " + x.TotalAnswers + " correct. " + Math.Round((double)x.CorrectAnswers/(double)x.TotalAnswers*100, 0) +"% w/ "+Math.Round(x.AvgResponseTimeMs/1000, 2)+" second response time.")),
                    Color = DiscordColor.Blue
                };
                await ctx.RespondAsync(embed: leaderboardEmbed);
            }   
        }

        [Command("stats_clear")]
        public async Task StatsClear(CommandContext ctx)
        {
            bool deleted = false;
            await ctx.RespondAsync("Are you sure you want to wipe your stats, " + ctx.Message.Author.Mention + "?");
            var interactivity = ctx.Client.GetInteractivity();
            await interactivity.WaitForMessageAsync(xm =>
            {
                if (ctx.Message.Author.Id.Equals(xm.Author.Id) && ctx.Message.Channel.Id.Equals(xm.Channel.Id))
                {
                    if(xm.Content.Trim().ToLower() == "yes")
                    {
                        Stats.Instance.DeletePlayerStats(xm.Author.Id);
                        deleted = true;
                        return true;
                    }
                }
                return false;
            }, TimeSpan.FromSeconds(10));
            if(deleted)
            {
                await ctx.RespondAsync("Your stats have been wiped!");
            }
            else
            {
                await ctx.RespondAsync("Your stats are still safe!");
            }
            
        }

        private async void SendQuestion(CommandContext ctx, Game game)
        {
            Question question = game.GetNextQuestion();
            
            DiscordEmbed embed = new DiscordEmbedBuilder
            {
                Author = new EmbedAuthor
                {
                    Name = question.category,
                    IconUrl = "http://www.pvhc.net/img25/kabobvfpbpjxllrhnqou.png"
                },
                Title = "Question "+(game.GetQuestionNumber()+1)+"/"+game.questions.Count()+":",
                Description = question.question,
                Color = DiscordColor.Green
            }
            .AddField("Options:", game.GetFormattedOptions());

            DiscordMessage originalMessage = await ctx.RespondAsync(embed: embed);

            // Get Current Timestamp
            long startTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            long gameID = game.Id;

            var interactivity = ctx.Client.GetInteractivity();
            await interactivity.WaitForMessageAsync(xm =>
            {
                if (int.TryParse(xm.Content.Trim(), out int result) && originalMessage.Channel.Id == xm.Channel.Id)
                {
                    bool alreadyAnswered = question.playerAnswers.ContainsKey(xm.Author.Username);

                    if(!alreadyAnswered)
                    {
                        bool isCorrect = question.SubmitAnswer(xm.Author.Username, int.Parse(xm.Content.Trim()));

                        OnPlayerAnswer_Event(ctx, xm, new PlayerAnswer
                        {
                            UserId = (long)xm.Author.Id,
                            Username = xm.Author.Username,
                            Category = question.category,
                            Difficulty = question.difficulty,
                            IsCorrect = isCorrect ? (short)1 : (short)0,
                            QuestionType = question.type,
                            EventTime = DateTime.UtcNow,
                            ResponseTimeMs = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() - startTime,
                            GameAuthorId = (long)game.AuthorId,
                            GameId = game.Id,
                            QuestionId = question.Id
                        });

                    }
                }
                return false;
            }, TimeSpan.FromSeconds(17));

            await ctx.RespondAsync(game.FormatQuestionAnswersForDiscord());

            if (!game.HasMoreQuestions())
            {
                if(game.questions.Count() > 1)
                {
                    
                    List<LeaderBoardResult> results = Stats.Instance.GetPlayerStatsForGame(gameID);
                    DiscordEmbed leaderboardEmbed = new DiscordEmbedBuilder
                    {
                        Author = new EmbedAuthor
                        {
                            Name = "Game Results",
                            IconUrl = "http://www.pvhc.net/img25/kabobvfpbpjxllrhnqou.png"
                        },
                        Title = "Leaderboard:",
                        Description = String.Join("\n", results.OrderByDescending(x => (double)x.CorrectAnswers / (double)x.TotalAnswers).Select(x => x.Username + ": " + x.CorrectAnswers + " of " + x.TotalAnswers + " correct. (AVG Response Time of " + Math.Round(x.AvgResponseTimeMs / 1000, 2) + " seconds)")),
                        Color = DiscordColor.Blue
                    };
                    await ctx.RespondAsync(embed: leaderboardEmbed);
                }

                DiscordEmbed finalEmbed = new DiscordEmbedBuilder
                {
                    Author = new EmbedAuthor
                    {
                        Name = question.category,
                        IconUrl = "http://www.pvhc.net/img25/kabobvfpbpjxllrhnqou.png"
                    },
                    Title = "Question:",
                    Description = question.question,
                    Color = DiscordColor.Red
                }
                .AddField("Options:", game.GetFormattedOptions());

                await originalMessage.ModifyAsync(embed: finalEmbed);
                GameManager.RemoveGame(ctx.Channel.Id);
            }

            await Task.Delay(2 * 1000);

            if (game.HasMoreQuestions())
            {
                SendQuestion(ctx, game);
            }
        }

        private static void HandlePlayerStats(CommandContext ctx, DiscordMessage m, PlayerAnswer playerAnswer)
        {
            Stats.Instance.InsertPlayerAnswer(playerAnswer);
        }

        private static async void HidePlayerAnswer(CommandContext ctx, DiscordMessage m, PlayerAnswer answerRecord)
        {
            try
            {
                await m.DeleteAsync();
            }
            catch(Exception e)
            {
                Console.Error.WriteLine("Message could not be deleted.");
            }
            await ctx.Client.SendMessageAsync(m.Channel, $"_{m.Author.Username} has submitted an answer._");
        }
    }
}
