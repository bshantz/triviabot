﻿
using TriviaBot.Plugins;

namespace TriviaBot.Trivia
{
    class TriviaPlugin : IBotPlugin
    {
        public string Name {
            get
            {
                return "Trivia Plugin";
            }
        }

        public void InitCommands(IBotPluginContext context)
        {
            context.GetCommandsNextExtension.RegisterCommands<TriviaCommands>();
        }
    }
}
