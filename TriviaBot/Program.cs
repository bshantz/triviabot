﻿using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.VoiceNext;
using System.IO;
using Microsoft.Extensions.Configuration;
using TriviaBot.Trivia.Stats;
using TriviaBot.Plugins;
using TriviaBot.Plugins.Jeopardy;
using DSharpPlus.Interactivity;
using System.Collections.Generic;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using TriviaBot.Plugins.DadJoke;
using TriviaBot.Plugins.RedditPlugin.Services;
using System;
using TriviaBot.Plugins.RedditPlugin;
//using TriviaBot.Plugins.Dictionary;

namespace TriviaBot
{
    class Program
    {
        static DiscordClient discord;
        static CommandsNextExtension commands;
        static InteractivityExtension interactivity;
        static VoiceNextExtension voice;
        static LavalinkNodeConnection lava;

        public static IConfiguration Configuration { get; set; }

        static void Main(string[] args)
        {
            MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory());
#if DEBUG
            builder.AddJsonFile("appsettings.json");
#else
            builder.AddJsonFile("appsettings.json");
#endif


            Configuration = builder.Build();

            Stats s = Stats.Instance;

            discord = new DiscordClient(new DiscordConfiguration
            {
                Token = Configuration["discordToken"],
                TokenType = TokenType.Bot,
                UseInternalLogHandler = true,
                LogLevel = LogLevel.Debug
            });


            // Setup Lavalink
            discord.UseLavalink();

            commands = discord.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefixes = new List<string> { Configuration["commandPrefix"] }, 
                EnableDms = false
            });

            //commands.RegisterCommands<Commands>();

            interactivity = discord.UseInteractivity(new InteractivityConfiguration
            {
            });

            voice = discord.UseVoiceNext(new VoiceNextConfiguration()
            {
                //VoiceApplication = DSharpPlus.VoiceNext.Codec.VoiceApplication.Music
                AudioFormat = AudioFormat.Default    
            });
            
            await discord.ConnectAsync();

            lava = null;
            if(Configuration["lavalinkHost"] != null && Configuration["lavalinkHost"] != "")
            {
                lava = await discord.GetLavalink().ConnectAsync(new LavalinkConfiguration
                {
                    RestEndpoint = new ConnectionEndpoint { Hostname = Configuration["lavalinkHost"], Port = int.Parse(Configuration["lavalinkPort"]) },
                    SocketEndpoint = new ConnectionEndpoint { Hostname = Configuration["lavalinkHost"], Port = int.Parse(Configuration["lavalinkPort"]) },
                    Password = Configuration["lavalinkPass"]
                });
            }
            

            // Run Plugins
            BotContext context = new BotContext(discord, interactivity, commands, lava, Configuration);

            //ExecutePlugin(context, new TriviaPlugin());
            ExecutePlugin(context, new JeopardyPlugin());
            ExecutePlugin(context, new DadJokePlugin());
            ExecutePlugin(context, new RedditPlugin());
            //ExecutePlugin(context, new AdminPlugin());
            //ExecutePlugin(context, new PassivePlugin());
            //if(Configuration["oxfordDictionaryAppID"] != null && Configuration["oxfordDictionaryAppKey"] != null)
            //{
            //    ExecutePlugin(context, new DictionaryPlugin(Configuration["oxfordDictionaryAppID"], Configuration["oxfordDictionaryAppKey"]));
            //}


            await Task.Delay(-1);
        }

        static void ExecutePlugin(BotContext context, IBotPlugin plugin)
        {
            plugin.InitCommands(context);
        }
    }
}
