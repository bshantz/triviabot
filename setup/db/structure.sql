create schema if not exists triviabot;

create table if not exists triviabot.player_answer (
	id bigserial primary key, 
	question_id integer, 
	game_id bigint, 
	user_id bigint, 
	username varchar(100), 
	category varchar(200), 
	question_type varchar(15), 
	difficulty varchar(15), 
	is_correct smallint, 
	event_time timestamp, 
	game_author_id bigint, 
	response_time_ms bigint 
);

create index if not exists game_id_idx on triviabot.player_answer (game_id);
create index if not exists user_id_idx on triviabot.player_answer (user_id);
create index if not exists category_idx on triviabot.player_answer (category);
